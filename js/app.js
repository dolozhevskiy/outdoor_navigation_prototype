(function () {
  'use strict';

  var CORDS = {
    'Kharkov': {
      latlng: {
        lat: 49.993500,
        lng: 36.230383
      }
    },
    'MD': [
      {
        latlng: {
          lat: 49.993500,
          lng: 36.230383
        }
      },
      {
        latlng: {
          lat: 50.013607,
          lng: 36.226044
        }
      },
      {
        latlng: {
          lat: 49.999055,
          lng: 36.240849
        }
      },
      {
        latlng: {
          lat: 49.992956,
          lng: 36.223390
        }
      },
      {
        latlng: {
          lat: 50.029045,
          lng: 36.328163
        }
      }
    ]
  };
  var CONFIG = {
    layer: {
      source: 'http://{s}.tile.osm.org/{z}/{x}/{y}.png',
      attr: {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      }
    },
    userMarkerOpt: {
      iconUrl: 'http://iconshow.me/media/images/Mixed/small-n-flat-icon/png2/512/-map-marker.png',
      iconSize: [41, 41]
    },
    zoomLvl: 13
  };
  var MAP, user;

  /**
   * Describe service for interaction with remote OSRM machine
   * @type {{}}
   */
  var OSRM = function() {
    this.server = 'https://router.project-osrm.org';

    this._formatLocs = function(latLngs) {
      return 'loc=' + latLngs.map(function(c) { return c.latlng.lat + ',' + c.latlng.lng; } ).join("&loc=");
    };

    this._request = function(service, encodedParams, callback) {
      $.get(this.server + '/' + service, encodedParams, callback);
    };

    this._getNearestPointLoc = function(points, cb) {
      // Find index of the smallest distance value
      var shortestDistIndex = function(data) {
        // Smallest dist
        var min = data.slice().sort(function(a,b) { return a - b })[1];

        return data.indexOf(min);
      };
      var requestHandler = function(res) {
        // Source of distance from point
        var src = res.distance_table[0];
        var result = res.destination_coordinates[shortestDistIndex(src)];
        var cords = {
          latlng: {
            lat: result[0],
            lng: result[1]
          }
        };

        cb(cords);
      };

      this._request('table', this._formatLocs(points), requestHandler);
    };

    // API methods
    this.getRoute = function(points, cb) {

      this._request('viaroute', this._formatLocs(points), function(res) {
        // decode response route
        var decodedRoute = L.PolylineUtil.decode(res.route_geometry, { factor:1e6 });

        cb(decodedRoute);
      });
    };

    this.getShortestRoute = function(startLoc, cb) {
      var allLoc = [startLoc].concat(CORDS.MD);

      this._getNearestPointLoc(allLoc, function(nearestLoc) {
        this.getRoute([startLoc, nearestLoc], cb)
      }.bind(this));
    };
  };

  /**
   * Describe user
   * @param mapInstance - Instance of Leaflet map
   * @constructor
   */
  var User = function(mapInstance) {
    this._map = mapInstance;
    this.currentLoc = null;
    this.markerIcon = L.icon(CONFIG.userMarkerOpt);
    this.router = new OSRM();
    this.marker = null;
    this.routeLayer = null;

    this.init();
  };

  User.prototype = {
    // Create route to marker that have been clicked.
    clickOnMarker: function(e) { this._buildRoute(e.latlng); },

    _findCurrentLoc: function() {
      // Try to find user position by HTML5 location.
      this._map.locate({setView: true, maxZoom: 13});

      // Listen for search location result
      this._map.on('locationfound', this._successHandler.bind(this))
               .on('locationerror', this._errorHandler.bind(this));
    },

    // Handlers for location search result
    _successHandler: function(e) {
      // Save user's location
      this.currentLoc = { latlng: e.latlng };

      this._setUserLocation(this.currentLoc);
      this._removeRoute();
      //this._createRoutToNearestPoint();
    },

    // TODO: add real error handler
    _errorHandler: function() { this.currentLoc = CORDS.Kharkov },

    // Set user location by click on the map
    _setUserLocation: function(e) {
      this.currentLoc = { latlng: e.latlng };

      if (this.marker) {
        this.marker.setLatLng(e.latlng);
      } else {
        // create marker
        this.marker = L.marker([e.latlng.lat, e.latlng.lng], { icon: this.markerIcon }).addTo(this._map);
      }

      this._removeRoute();
    },

    _buildRoute: function(pointCords) {
      var points = [this.currentLoc, { latlng: pointCords }];

      if (this.currentLoc) {
        this.router.getRoute(points, this._drawRoute.bind(this));
      } else {
        alert('First define your position on the map');
      }
    },

    _buildShortestRoute: function() {
      if (this.currentLoc) {
        this.router.getShortestRoute(this.currentLoc, this._drawRoute.bind(this));
      } else {
        alert('First define your position on the map');
      }
    },

    _drawRoute: function(route) {
      // remove previous drawn layer
      this._removeRoute();

      this.routeLayer = new L.polyline(route);
      this._map.addLayer(this.routeLayer);
      //this._map.fitBounds(this.routLayer.getBounds());
    },

    _removeRoute: function() {
      if(this.routeLayer) this._map.removeLayer(this.routeLayer);
    },

    init: function() {
      // Handlers for interaction with map
      this._map.on('click', this._setUserLocation.bind(this));

      // Handlers for control buttons
      $('#shortestRoute').click(this._buildShortestRoute.bind(this));
      $('#findMe').click(this._findCurrentLoc.bind(this));
    }
  };

  /**
   * Application initialization
   */

  // Load map
  MAP = L.map( document.getElementById('map') );

  // Init map layer
  L.tileLayer(CONFIG.layer.source, CONFIG.layer.attr).addTo(MAP);

  // Set view params
  MAP.setView([CORDS.Kharkov.latlng.lat, CORDS.Kharkov.latlng.lng], CONFIG.zoomLvl);

  // Create new user
  user = new User(MAP);

  // Helper method
  function setStaticMarkers(cords) {
    cords.forEach(function(point) {
      L.marker([point.latlng.lat, point.latlng.lng])
        .addTo(MAP)
        .on('click', user.clickOnMarker.bind(user));
    })
  }

  // Set markers for each Macdonald's restaurant in Kharkov
  setStaticMarkers(CORDS.MD);
})();
